from sqlalchemy import create_engine, Column, Integer, String
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

# Create an SQLite database in memory
engine = create_engine('sqlite:///:memory:', echo=True)

# Create a session factory
Session = sessionmaker(bind=engine)

# Create a base class for declarative models
Base = declarative_base()

# Define a simple User class
class User(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    age = Column(Integer)

# Create the tables in the database
Base.metadata.create_all(engine)

# Create a session
session = Session()

# Add new users to the database
user1 = User(name='Alice', age=25)
user2 = User(name='Bob', age=30)
user3 = User(name='Wai Yan', age=16)

session.add(user1)
session.add(user2)
session.add(user3)
session.commit()

# Query the database
users = session.query(User).all()

# Print the retrieved users
for user in users:
    print(f"User ID: {user.id}, Name: {user.name}, Age: {user.age}")

# Close the session
session.close()
