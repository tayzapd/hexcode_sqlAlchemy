from sqlalchemy import create_engine, Column, Integer, String
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

# Create an SQLite database in memory
engine = create_engine('sqlite:///crud_example.db', echo=True)

# Create a session factory
Session = sessionmaker(bind=engine)

# Create a base class for declarative models
Base = declarative_base()

# Define a simple User class
class User(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    age = Column(Integer)

# Create the tables in the database
Base.metadata.create_all(engine)

# Create a session
session = Session()

# Create
def create_user(name, age):
    user = User(name=name, age=age)
    session.add(user)
    session.commit()
    print(f"User {name} added successfully!")

# Read
def get_users():
    users = session.query(User).all()
    return users

# Update
def update_user(user_id, new_name, new_age):
    user = session.query(User).filter_by(id=user_id).first()
    if user:
        user.name = new_name
        user.age = new_age
        session.commit()
        print(f"User {user_id} updated successfully!")
    else:
        print(f"User {user_id} not found.")

# Delete
def delete_user(user_id):
    user = session.query(User).filter_by(id=user_id).first()
    if user:
        session.delete(user)
        session.commit()
        print(f"User {user_id} deleted successfully!")
    else:
        print(f"User {user_id} not found.")

# Perform CRUD operations
create_user("Alice", 25)
create_user("Bob", 30)

print("Initial Users:")
users = get_users()
for user in users:
    print(f"User ID: {user.id}, Name: {user.name}, Age: {user.age}")

update_user(1, "Alicia", 26)
delete_user(2)

print("Updated Users:")
users = get_users()
for user in users:
    print(f"User ID: {user.id}, Name: {user.name}, Age: {user.age}")

# Close the session
session.close()
