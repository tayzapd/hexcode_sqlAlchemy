from sqlalchemy import create_engine, Column, Integer, String
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
import os 

engine = create_engine('sqlite:///hex_code.db', echo=True)

Session = sessionmaker(bind=engine)

Base = declarative_base()


class User(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    age = Column(Integer)


Base.metadata.create_all(engine)
session = Session()

class App:
    def __init__(self):
        self.session = session
        self.users = []
    
    def clearScreen(self):
        os.system("clear")

    def showMenu(self):
        print("="*30)
        print("SHOW ALL USERS - 1")
        print("CREATE USER    - 2")
        print("UPDATE USERS   - 3")
        print("DELETE USER    - 4")
        print("EXIT           - 99")
        print("="*30)

    def mainEngine(self):
        try:
            option = int(input("Enter your option >> "))
            functions = {
                1: self.showAllUsers,
                2: self.addUser,
                3: self.updateUser,
                4: self.deleteUser,
                99: exit
            }

            function = functions.get(option)
            if function:
                function()
        except KeyboardInterrupt: 
            print()
            print("Wrong option!")
        
    
    def showAllUsers(self):
        self.users = session.query(User).all()
        for user in self.users:
            print("-"*30)
            print('id : {} | name : {}  | age : {} '.format(user.id,user.name,user.age))
    
    def addUser(self):
        name = input("Name >> ")
        age_input = input("Age >> ")

        if not name:
            print("Please enter a name.")
            return

        try:
            age = int(age_input)
            if age < 0:
                print("Please enter a valid age.")
                return
        except ValueError:
            print("Please enter a valid age.")
            return

        user = User(name=name, age=age)
        session.add(user)
        session.commit()
        print("User added successfully!")

    def updateUser(self):
        user_id = int(input("Enter the id >> "))

        try:
            user = session.query(User).filter_by(id=user_id).first()
        except Exception as e:
            print("Error getting user:", e)
            return

        if not user:
            print(f"User {user_id} not found.")
            return
        self.clearScreen()
        print("-"*30)
        print("Name >> {} , Age >> {}".format(user.name,user.age))
        print("-"*30)

        user.name = input("Name >> ")
        user.age = int(input("Age  >> "))

        session.commit()
        self.clearScreen()
        print(f"User {user_id} updated successfully!")
        self.showAllUsers()

    def deleteUser(self):
        user_id = int(input("Enter the id >> "))

        try:
            user = session.query(User).filter_by(id=user_id).first()
        except Exception as e:
            print("Error getting user:", e)
            return

        if not user:
            print(f"User {user_id} not found.")
            return

        self.clearScreen()
        print("-"*30)
        print("Name >> {} , Age >> {}".format(user.name,user.age))
        print("-"*30)

        confirmation = input("Are you sure you want to delete this user? (y/n) >> ")

        if confirmation == "y":
            session.delete(user)
            session.commit()
            self.clearScreen()
            print(f"User {user_id} deleted successfully!")
            self.showAllUsers()
        else:
            print("User not deleted.")


def main():
    app = App()
    while True:
        app.showMenu()
        app.mainEngine()




main()



